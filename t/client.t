use Test::Most tests => 3;

use_ok('WWW::BitBucket');

subtest "username & password are optional" => sub {
    my $client = WWW::BitBucket->new();

    isa_ok($client, 'WWW::BitBucket');
    ok !$client->_ua->default_header('Authorization');
};

subtest "use the username & password if provided" => sub {
    my $client = WWW::BitBucket->new(
        username => 'username',
        password => 'password',
    );
    isa_ok($client, 'WWW::BitBucket');
    ok $client->_ua->default_header('Authorization'), 'Authorization header set';
};
