use Test::Most;

{
    package Mock::WWW::BitBucket;

    use Moo;

    has 'replies' => (
        is => 'ro',
        default => sub { +{} },
    );

    sub get {
        my ($self, $url) = @_;

        return $self->replies->{$url}
            or die "404 Not Found\n$url";
    }
};

my @tests = (
    {
        label   => 'Empty page',
        replies => {
            'page 1' => {
                values => [],
            },
        },
        expected => [],
    },

    {
        label   => 'Single page',
        replies => {
            'page 1' => {
                values => [1,2,3,4,5],
            },
        },
        expected => [1,2,3,4,5],
    },

    {
        label   => 'Many pages',
        replies => {
            'page 1' => {
                next   => 'page 2',
                values => [1,2,3,4,5],
            },
            'page 2' => {
                next   => 'page 3',
                values => [6,7,8,9,10],
            },
            'page 3' => {
                values => [11,12,13,14,15],
            },
        },
        expected => [1..15],
    },

);

plan tests => scalar(@tests) + 1;

use_ok 'WWW::BitBucket::Iterator';

foreach my $test ( @tests ) {
    my $mock_client = Mock::WWW::BitBucket->new(
        replies => $test->{replies},
    );

    my $iterator = WWW::BitBucket::Iterator->new(
        url    => 'page 1',
        client => $mock_client,
    );

    my @results;
    while ( my $result = $iterator->next() ) {
        push @results, $result;
    }

    is_deeply \@results, $test->{expected}, $test->{label};
}

done_testing();
