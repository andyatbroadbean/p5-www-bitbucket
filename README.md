# WWW::BitBucket

Query the [BitBucket REST API](https://confluence.atlassian.com/display/BITBUCKET/Use+the+Bitbucket+REST+APIs)

Only consists of the building blocks:

 * WWW::BitBucket - authentication and makes requests to the API
 * WWW::BitBucket::Iterator - convenience interface for dealing with paged responses

TODO:

 * LOTS!
 * Add POST/PUT/DELETE support
 * Add convenience methods to hide the urls
 * Docs
