package WWW::BitBucket;

# ABSTRACT: Access the Bitbucket APIs

# Docs: https://confluence.atlassian.com/display/BITBUCKET/Use+the+Bitbucket+REST+APIs

use Moo;

use LWP::UserAgent;
use MIME::Base64 'encode_base64';
use JSON;

has [qw/username password/] => ( is => 'ro' );

has _ua => (
    is      => 'ro',
    lazy    => 1,
    builder => '_build_ua',
);

sub _build_ua {
    my ($self) = @_;


    my $lwp = LWP::UserAgent->new();

    if ( $self->username ) {
        my $auth_header = _http_basic_auth($self->username, $self->password);
        $lwp->default_header('Authorization' => $auth_header);
    }

    return $lwp;
}

sub get {
    my ($self, $url) = @_;

    my $resp = $self->_ua->get($url);

    if ( !$resp->is_success() ) {
        die join("\n\n", "Request to '$url' failed:",
                         $resp->request->as_string(),
                         $resp->as_string());
    }

    my $json = JSON::from_json($resp->decoded_content());
    return $json;
}

# Static methods
sub v1_url {
    shift; # discard class/self
    return join('/', 'https://bitbucket.org/api/1.0', @_);
}

sub v2_url {
    shift; # discard class/self
    return join('/', 'https://bitbucket.org/api/2.0', @_);
}

# Helpers
sub _http_basic_auth {
    my ($username, $password) = @_;

    my $base64 = encode_base64($username.':'.$password, "");

    return 'Basic '.$base64;
}

1;
