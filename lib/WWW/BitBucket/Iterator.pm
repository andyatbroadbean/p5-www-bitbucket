package WWW::BitBucket::Iterator;

# Iterate over Bitbucket's paged responses

use Moo;

has 'client' => ( is => 'ro' );
has 'url'    => ( is => 'ro' );

has '_json'  => (
    is => 'rw',
    lazy => 1,
    builder => '_build_json',
);

sub _build_json {
    my $self = shift;
    return $self->fetch_page($self->url());
}

sub next {
    my $self = shift;

    if ( my $next = $self->next_item() ) {
        return $next;
    }

    if ( $self->fetch_next_page() ) {
        return $self->next_item();
    }

    return;
}

sub next_item {
    my $self = shift;
    return shift @{ $self->_json->{values} };
}

sub fetch_next_page {
    my $self = shift;

    my $next_page = $self->_json->{next}
        or return;

    return $self->fetch_page($next_page);
}

sub fetch_page {
    my ($self, $url) = @_;

    my $json = $self->client->get($url);
    return $self->_json($json);
}

1;
