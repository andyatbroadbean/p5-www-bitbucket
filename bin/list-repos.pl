#!/usr/bin/env perl

# List the bitbucket repos associated with a username

use strict;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Getopt::Long;
use WWW::BitBucket;
use WWW::BitBucket::Iterator;

GetOptions(
    'username=s' => \my $bb_username, # Your bitbucket username
    'password=s' => \my $bb_password, # Your bitbucket password
    'account=s'  => \my $team,
);

$team ||= $bb_username
    or die "Please specify an account to list";

if ( $bb_username && !$bb_password ) {
    require Term::ReadKey; # only load if we need it

    print "Bitbucket password: ";
    Term::ReadKey::ReadMode(2);
    chomp($bb_password = <STDIN>);
    Term::ReadKey::ReadMode(0);
    print "\n";
}

my $client = WWW::BitBucket->new(
    username => $bb_username,
    password => $bb_password,
);

my $repo_iter = WWW::BitBucket::Iterator->new(
    client => $client,
    url    => $client->v2_url('teams', $team, 'repositories'),
);

while ( my $repo = $repo_iter->next() ) {
    print join(' / ', $repo->{owner}->{username}, $repo->{name}), "\n";
}
