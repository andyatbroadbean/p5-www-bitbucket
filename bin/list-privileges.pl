#!/usr/bin/env perl

# List the priliviges associated with all the account's repositories

use strict;
use FindBin;
use lib "$FindBin::RealBin/../lib";

use Getopt::Long;
use WWW::BitBucket;

GetOptions(
    'username=s' => \my $bb_username, # Your bitbucket username
    'password=s' => \my $bb_password, # Your bitbucket password
    'account=s'  => \my $team,
    'filter=s'   => \my $filter,      # Optional filter
);

$team ||= $bb_username
    or die "Please specify an account to list";

if ( $bb_username && !$bb_password ) {
    require Term::ReadKey; # only load if we need it

    print "Bitbucket password: ";
    Term::ReadKey::ReadMode(2);
    chomp($bb_password = <STDIN>);
    Term::ReadKey::ReadMode(0);
    print "\n";
}

my $client = WWW::BitBucket->new(
    username => $bb_username,
    password => $bb_password,
);

my $privileges_url = $client->v1_url('privileges', $team);
foreach my $privilege ( @{ $client->get($privileges_url) } ) {
    next if $filter && $privilege->{user}->{username} ne $filter;

    printf "%s\t%s\t%s\n", $privilege->{repo}, $privilege->{user}->{username}, $privilege->{privilege};
}
